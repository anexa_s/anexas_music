from music.user import User
from music.music_player import MusicPlayer
from music.song_manager import SongManager
from music.playlist import Playlist
import argparse


def music_commands(music_parser):
    group = music_parser.add_mutually_exclusive_group()
    group.add_argument(
            "-p", "--play",
            help="to play a playlist on terminal",
            nargs=1,
            action="store"  # add a playlist
    )

    group.add_argument(
        "-d", "--download",
        help="to download a song with the provided url from youtube",
        nargs=1,
        action="store"  # add a youtube url
    )

    group.add_argument(
        "-a", "--add",
        help="to add song to a playlist",
        nargs=2,
        action="store"  # add a song to a playlist
    )

    group.add_argument(
        "-r", "--remove",
        help="to remove song from a playlist",
        nargs=2,
        action="store"  # remove a song from a playlist
    )

    group.add_argument(
        "-l", "--list",
        help="to list all the available songs in a playlist",
        nargs=1,
        action="store"  # list songs in a playlist
    )

    group.add_argument(
        "-c", "--create",
        help="to create a new playlist",
        nargs=1,
        action="store"  # to create a new playlist
)


def main():
    parser = argparse.ArgumentParser(description='anexas music player')
    music_commands(parser)

    args = parser.parse_args()
    user = User.get_instance()
    if args.play:
        playlist_title = args.play[0]
        music_player = MusicPlayer(playlist_title)
        music_player.launch()
    elif args.download:
        url = args.download[0]
        SongManager.download_song(url)
    elif args.add:
        song = args.add[0]
        playlist_title = args.add[1]
        playlist = Playlist(playlist_title)
        playlist.add_song(song)
    elif args.remove:
        song = args.remove[0]
        playlist_title = args.remove[1]
        playlist = Playlist(playlist_title)
        playlist.delete_song(song)
    elif args.list:
        playlist_title = args.list[0]
        playlist = Playlist(playlist_title)
        print(playlist_title)
        for song in playlist.fetch_playlist():
            print(song)
    elif args.create:
        playlist_title = args.create[0]
        playlist = Playlist(playlist_title)


if __name__ == '__main__':
    main()



