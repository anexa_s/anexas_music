from abc import ABC, abstractmethod
import random


class Arranger(ABC):

    def __init__(self):
        self._songs = []

    def set_songs(self, songs):
        self._songs = songs

    def get_songs(self):
        return self._songs

    @abstractmethod
    def arrange(self):
        pass


class RandomArranger(Arranger):
    def arrange(self):
        random.shuffle(self._songs)
        print("Arranged randomly.")
        return self._songs
