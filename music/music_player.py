import curses
import threading

from music.utils import SetInterval
from music.player import Player
from music.playlist import Playlist
from music.arranger import RandomArranger


class MusicPlayer:
    """UI for the MusicPlayer"""
    anexa = """
      _        ____  _____   ________   ____  ____        _
     / \      |_   \|_   _| |_   __  | |_  _||_  _|      / \\
    / _ \       |   \ | |     | |_ \_|   \ \  / /       / _ \\
   / ___ \      | |\ \| |     |  _| _     > `' <       / ___ \\
 _/ /   \ \_   _| |_\   |_   _| |__/ |  _/ /'`\ \_   _/ /   \ \\_
|____| |____| |_____|\____| |________| |____||____| |____| |____|
"""

    def __init__(self, title):
        self.__menu = ['Play/Pause', 'Next', 'Forward', 'Backward', 'Exit']
        self.__playlist = Playlist(title)
        self.__arranger = RandomArranger()

        songs = self.fetch_songs()
        self.__player = Player(songs)

        self.__running = True
        self.__loader = None

    def get_songs(self):
        return self.__arranger.get_songs()

    def fetch_songs(self):
        songs = self.__playlist.fetch_playlist()
        self.__arranger.set_songs(songs)
        return self.__arranger.arrange()

    def set_arranger(self, arranger):
        self.__arranger = arranger
        songs = self.fetch_songs()
        self.__player.set_queue(songs)

    def print_title(self, stdscr):
        (height, width) = stdscr.getmaxyx()
        stdscr.addstr(0, width // 2, self.anexa)
        stdscr.refresh()

    def print_menu(self, stdscr, menu_idx, active):
        """Prints the menu for the player"""
        (height, width) = stdscr.getmaxyx()
        for (idx, menu_item) in enumerate(self.__menu):
            menu_item_x = width - len(menu_item) - 1
            menu_item_y = height // 2 - len(self.__menu)//2 + idx
            if idx == menu_idx and active:
                stdscr.attron(curses.color_pair(1))
                stdscr.addstr(menu_item_y, menu_item_x, menu_item)
                stdscr.attroff(curses.color_pair(1))
            else:
                stdscr.addstr(menu_item_y, menu_item_x, menu_item)

        stdscr.refresh()

    def print_loader(self, stdscr, percentage_calc, grain):
        """Prints the percentage of song done"""
        (height, width) = stdscr.getmaxyx()
        percentage = int(percentage_calc() / grain) * grain
        block = "\u2588"
        for i in range(0, percentage, grain):
            stdscr.addstr(height - 1, 1 + i // grain, block)
        stdscr.attron(curses.color_pair(2))
        for i in range(percentage, 100, grain):
            stdscr.addstr(height - 1, 1 + i // grain, block)
        stdscr.attroff(curses.color_pair(2))
        stdscr.addstr(height - 1, 2 + 100 // grain, "{:6.2f}% complete".format(percentage))
        stdscr.refresh()

    def print_playlist(self, stdscr, playlist_idx, active):
        """Prints the playlist for the player"""
        (height, width) = stdscr.getmaxyx()
        limit = 2

        for (idx, song) in enumerate(self.get_songs()):
            if abs(idx - playlist_idx) <= limit:
                song_x = 1
                song_y = height // 2 + idx - playlist_idx
                if idx == playlist_idx and active:
                    stdscr.attron(curses.color_pair(1))
                    stdscr.addstr(song_y, song_x, song[:-4][:int(0.7 * width)] + "...")
                    stdscr.attroff(curses.color_pair(1))
                elif idx == self.__player.get_current_idx():
                    stdscr.attron(curses.color_pair(2))
                    stdscr.addstr(song_y, song_x, song[:-4][:int(0.7 * width)] + "...")
                    stdscr.attroff(curses.color_pair(2))
                else:
                    stdscr.addstr(song_y, song_x, song[:-4][:int(0.7 * width)] + "...")

        stdscr.refresh()

    def take_action(self, menu_idx, playlist_idx, player_idx):
        menu = self.__menu
        if player_idx == 1:
            if menu[menu_idx] == 'Exit':
                self.__running = False
                if self.__loader:
                    self.__loader.cancel_event()
                if self.__player.get_loader():
                    self.__player.get_loader().cancel_event()
            elif menu[menu_idx] == 'Play/Pause':
                self.__player.pause()
            elif menu[menu_idx] == 'Next':
                self.__player.next()
            elif menu[menu_idx] == 'Forward':
                self.__player.forward()
        else:
            self.__player.play(playlist_idx)

    @staticmethod
    def check_dimensions(stdscr):
        (height, width) = stdscr.getmaxyx()
        if height < 21:
            print("Screen too short in height: {} units. It must be atleast 21 units".format(height))
            return False
        if width < 75:
            print("Screen too narrow in width: {} units. It must be atleast 75 units".format(width))
            return False
        return True

    def draw_components(self, stdscr, playlist_idx, menu_idx, player_idx):
        stdscr.clear()
        self.print_title(stdscr)
        self.print_menu(stdscr, menu_idx, player_idx == 1)
        self.print_playlist(stdscr, playlist_idx, player_idx == 0)
        self.print_loader(stdscr, self.__player.percentage_complete, 2)

    def start_player(self, stdscr):
        """Main UI function of the player"""
        # no curser blinking on the screen
        curses.curs_set(0)

        # colors
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
        curses.init_pair(2, curses.COLOR_BLUE, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_RED, curses.COLOR_BLACK)

        # location pointers
        menu_idx = 0
        playlist_idx = 0
        player_idx = 1

        # check dimensions
        if not self.check_dimensions(stdscr):
            return

        # draw stuff
        stdscr.clear()
        self.print_menu(stdscr, menu_idx, player_idx == 1)
        self.print_playlist(stdscr, playlist_idx, player_idx == 0)
        self.__loader = SetInterval(0.01, self.print_loader, (stdscr, self.__player.percentage_complete, 2))
        self.print_loader(stdscr, self.__player.percentage_complete, 2)
        self.print_title(stdscr)

        # flag
        self.__running = True
        while self.__running:
            key = stdscr.getch()
            stdscr.clear()

            # take appropriate action
            if key == curses.KEY_UP:
                menu_idx = (len(self.__menu) + menu_idx - 1 * (player_idx == 1)) % len(self.__menu)
                playlist_idx = (len(self.get_songs()) + playlist_idx - 1 * (player_idx == 0)) % len(self.get_songs())
            elif key == curses.KEY_DOWN:
                menu_idx = (menu_idx + 1 * (player_idx == 1)) % len(self.__menu)
                playlist_idx = (playlist_idx + 1 * (player_idx == 0)) % len(self.get_songs())
            elif key in [curses.KEY_RIGHT, curses.KEY_LEFT]:
                player_idx = 1 - player_idx
            elif key == curses.KEY_ENTER or key in [10, 13]:
                take_action_thread = threading.Thread(target=self.take_action, args=(menu_idx, playlist_idx, player_idx,))
                take_action_thread.start()

            # draw updated stuff
            self.print_menu(stdscr, menu_idx, player_idx == 1)
            self.print_playlist(stdscr, playlist_idx, player_idx == 0)
            self.print_loader(stdscr, self.__player.percentage_complete, 2)
            self.print_title(stdscr)

    def launch(self):
        """Launches the player"""
        curses.wrapper(self.start_player)
