import pyglet

from music.settings import *
from music.utils import join, SetInterval


class Player:
    """Music Player"""
    def __init__(self, songs):
        self.__player = pyglet.media.Player()

        self.__current_idx = 0
        self.__current_song_duration = 0

        self.__queue = songs

        self.__loader = None

    def get_current_idx(self):
        return self.__current_idx

    def get_loader(self):
        return self.__loader

    def set_queue(self, songs):
        if self.__player.source:
            self.__player.pause()
            self.__player.next_source()
            print("Paused: {}.".format(self.__queue[self.__current_idx]))
        self.__current_idx = 0
        self.__current_song_duration = 0
        self.__queue = songs

    def play(self, idx):
        if self.__player.source:
            self.__player.pause()
            self.__player.next_source()
            print("Paused: {}.".format(self.__queue[self.__current_idx]))

        self.__current_idx = idx
        self.queue_song(self.__queue[idx])
        self.__player.play()
        print("Playing: {}".format(self.__queue[self.__current_idx]))
        if self.__loader:
            self.__loader.cancel_event()
        self.__loader = SetInterval(self.__current_song_duration, self.next, ())
        print("Next song in: {}".format(self.__current_song_duration))

    def pause(self):
        if self.__player.playing:
            self.__loader.cancel_event()
            self.__player.pause()
            print("Paused: {}.".format(self.__queue[self.__current_idx]))
        else:
            self.__player.play()
            print("Playing: {}.".format(self.__queue[self.__current_idx]))
            self.__loader = SetInterval(self.__current_song_duration - self.__player.time, self.next, ())
            print("Next song in: {}".format(self.__current_song_duration))

    def queue_song(self, song):
        song_link = join(SONGS, song)
        print("Loading song {}....".format(song))
        source = pyglet.media.load(song_link, streaming=False)
        self.__current_song_duration = source.duration
        self.__player.queue(source)

    def next(self):
        self.play((self.__current_idx + 1) % len(self.__queue))

    def forward(self):
        self.__player.seek(self.__player.time + 10)
        self.__player.play()

    def backward(self):
        self.__player.seek(self.__player.time - 10)
        self.__player.play()

    def percentage_complete(self):
        if self.__current_song_duration == 0:
            return 100.00
        else:
            return (self.__player.time / self.__current_song_duration) * 100
