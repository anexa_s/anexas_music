from music.user import User
from music.settings import *
from music.utils import join, connect_to_db, create_table_if_not_exists, now


class Playlist:

    def __init__(self, title):
        self.__title = title
        user = User.get_instance()
        self.__user_id = user.user_id
        self.__full_title = self.__user_id + "_" + self.__title
        playlist = join(join(DATA, self.__user_id), title + '.db')
        self.__connection, self.__cursor = connect_to_db(playlist)
        print("Connection to {} database successful...".format(title))
        create_table_if_not_exists(self.__connection, self.__cursor, self.__full_title, """
            id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
            name text NOT NULL,
            count integer default 0 NOT NULL,
            time timestamp NOT NULL
        """)

    def add_song(self, song):
        with self.__connection as conn:
            result = self.__cursor.execute("""
                SELECT * FROM {}
                WHERE name=?;
            """.format(self.__full_title), (song,)).fetchall()
            if not len(result):
                self.__cursor.execute("""
                    INSERT INTO {} (name, time) VALUES (?, ?);
                """.format(self.__full_title),  (song, now()))
                conn.commit()
                print("song {} successfully added to {}".format(song, self.__title))
            else:
                print("song {} already present in {}".format(song, self.__title))

    def delete_song(self, song):
        with self.__connection as conn:
            self.__cursor.execute("""
                DELETE FROM {}
                WHERE name=?;
            """.format(self.__full_title), (song,))
            conn.commit()
            print("song {} successfully removed from {}".format(song, self.__title))

    def fetch_playlist(self):
        with self.__connection:
            result = self.__cursor.execute(""" 
                SELECT name FROM {};
            """.format(self.__full_title)).fetchall()
            songs = []
            if len(result):
                for song in result:
                    songs.append(song[0])
            return songs
