from music.utils import join, environ


DATA = environ('MUSIC_DATA')
USERS = join(DATA, 'users.db')
COOKIE = join(DATA, 'cookie.pickle')
SONGS = join(DATA, 'songs')
LYRICS = join(DATA, 'lyrics')
