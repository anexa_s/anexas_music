import youtube_dl

from music.settings import *
from music.utils import join, listdir, isfile, exists


class SongManager:

    def __init__(self):
        pass

    @staticmethod
    def list_downloaded_songs():
        downloads = SONGS
        files = []
        for file in listdir(downloads):
            file_path = join(downloads, file)
            if isfile(file_path):
                files.append(file)
        return files

    @staticmethod
    def download_song(song):
        if not SongManager.find_song(song):
            ydl_opts = {
                'format': 'bestaudio/best',
                'outtmpl': join(SONGS, '%(title)s-%(id)s.%(ext)s'),
                'postprocessors': [{
                    'key': 'FFmpegExtractAudio',
                    'preferredcodec': 'mp3',
                    'preferredquality': '192',
                }],
            }
            ydl = youtube_dl.YoutubeDL(ydl_opts)
            try:
                ydl.download([song])
            except Exception as e:
                print("Error: {}".format(e))
                exit()
            print("Download successful.")
        else:
            print("Song already downloaded.")

    @staticmethod
    def find_song(song):
        print("Checking if file already downloaded or not...")
        ydl_opts = {
            'outtmpl': join(SONGS, '%(title)s-%(id)s.mp3'),
        }
        ydl = youtube_dl.YoutubeDL(ydl_opts)
        info_dict = ydl.extract_info(song, download=False)
        song_path = ydl.prepare_filename(info_dict)

        return exists(song_path)
