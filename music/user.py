from getpass import getpass

from music.settings import *
from music.utils import exists, remove,\
                  connect_to_db, hash5, create_table_if_not_exists, \
                  pickle_dump, pickle_load


class User:

    user = None

    def __init__(self):
        self.__connection, self.__cursor = connect_to_db(USERS)
        print("Connection to users database successful...")
        create_table_if_not_exists(self.__connection, self.__cursor, "USERS", """
            username text PRIMARY KEY NOT NULL,
            password text NOT NULL
        """)
        self.user_id = self.get_cookie()

    def get_cookie(self):
        if exists(COOKIE):
            data = pickle_load(COOKIE)
            print("Logged in as {}".format(data['username']))
            return data['username']
        else:
            while True:
                prompt = input("Login/Signup [L/S]: ")
                if prompt == 'L':
                    return self.login()
                elif prompt == 'S':
                    return self.signup()
                else:
                    print('Are you stupid!!')

    def login(self):
        print("...LOGIN...\n")
        user_name = input('Username: ')
        password = hash5(getpass('Password: '))
        with self.__connection:
            result = self.__cursor.execute("""
                SELECT * FROM USERS
                WHERE username=? AND password=?;
            """, (user_name, password)).fetchall()
            if not len(result):
                print("Login failed invalid username or password.")
                exit()
        self.create_cookie(user_name, password)
        print("Login successful.")
        return user_name

    def logout(self):
        print("..LOGOUT..\n")
        self.user_id = None
        remove(COOKIE)
        print("Logout successful.")

    @staticmethod
    def create_cookie(user_name, password):
        pickle_dump(COOKIE, {'username': user_name, 'password': password})
        print("New session created for {}".format(user_name))

    def signup(self):
        print("...SIGNUP...\n")
        user_name = input('Username: ')
        password = hash5(getpass('Password: '))
        with self.__connection as conn:
            result = self.__cursor.execute("""
                SELECT * FROM USERS
                WHERE username=?;
            """, (user_name, )).fetchall()
            if len(result):
                print("User already exists.")
                exit()
            self.__cursor.execute("""
                INSERT INTO USERS (username, password) VALUES (?,?);
            """, (user_name, password))
            conn.commit()
        self.create_cookie(user_name, password)
        print("Signup successful.")
        return user_name

    def signout(self):
        print("..SIGNOUT...")
        if not self.user_id:
            print("Login/Signin first.")
        else:
            with self.__connection as conn:
                self.__cursor.execute("""
                                DELETE FROM USERS
                                WHERE username=?;
                            """, (self.user_id,))
                conn.commit()
            remove(COOKIE)
            print("Signout successful.")

    def all_users(self):
        with self.__connection:
            result = self.__cursor.execute("""
                       SELECT * FROM USERS;
                   """).fetchall()
            if len(result):
                for row in result:
                    print(row)

    @staticmethod
    def get_instance():
        if not User.user:
            User.user = User()
        return User.user
