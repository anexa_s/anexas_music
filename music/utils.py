import os
import sqlite3
import hashlib
import pickle
import datetime
import time
import threading
from hurry.filesize import size


def file_size(file_name):
    return size(file_name)


def hash5(val):
    return hashlib.md5(val.encode()).hexdigest()


# datatime utils
def now():
    return datetime.datetime.now()


# SQLite utils
def connect_to_db(db):
    try:
        connection = sqlite3.connect(db)
        cursor = connection.cursor()
        return connection, cursor
    except Exception as e:
        print('DB connection error', e)
        exit()


def create_table_if_not_exists(connection, cursor, table, entries):
    with connection as conn:
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS {} (
            {}
        );""".format(table, entries))
        conn.commit()


# OS utils
def exists(path):
    return os.path.exists(path)


def environ(variable):
    return os.environ.get(variable)


def mkdir(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except OSError as err:
            print('Cannot create path: {} '.format(path), err)
            exit()


def remove(file):
    os.remove(file)


def join(path, file):
    mkdir(path)
    return os.path.join(path, file)


def listdir(directory):
    return os.listdir(directory)


def isfile(file_name):
    return os.path.isfile(file_name)


# pickle utils
def pickle_dump(file_name, data):
    with open(file_name, "wb") as dump:
        pickle.dump({'data': data}, dump)


def pickle_load(file_name):
    with open(file_name, "rb") as dump:
        data = pickle.load(dump)
        return data['data']


class SetInterval:
    def __init__(self, interval, event, args):
        self.interval = interval
        self.event = event
        self.args = args
        self.stop_event = threading.Event()
        thread = threading.Thread(target=self.set_interval, args=self.args)
        thread.start()

    def set_interval(self, *args, **kwargs):
        next_time = time.time() + self.interval
        while not self.stop_event.wait(next_time - time.time()):
            next_time += self.interval
            self.event(*args, **kwargs)

    def cancel_event(self):
        self.stop_event.set()
