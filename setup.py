from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='music',
    version='0.0.1',
    description='Software project',
    long_description=readme(),
    long_description_content_type='text/markdown',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent'
    ],
    url='https://gitlab.com/anexa_s/anexas_music.git',
    author='ktk54x',
    author_email='kartikeya.0005@gmail.com',
    keywords='Software',
    packages=['music'],
    install_requires=[
        'youtube-dl==2021.6.6',
        'pyglet==1.5.17',
        'hurry.filesize==0.9',
        'windows-curses==2.2.0'
    ],
    include_package_data=True,
    entry_points={
            'console_scripts': [
                'anexas_music = music.__main__:main'
            ]
    }
)
